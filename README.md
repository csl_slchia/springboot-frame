# springboot-frame

#### 项目介绍
前后端分离框架，支持集群式服务(这里不是分布式模式)

#### 软件架构
基于springboot2.0.1版本的一个快速开发平台.<br>
		1.集成了mybatis <br>
		2.集成了mybatis 通用mapper插件.<br>
		3.集成了mybatis-generator 代码生成器插件<br>
		4.mybatis分页插件，结合自定义aop注解，分页查询一个注解搞定<br>
		5.使用最新的日志管理log4j2版本<br>
		6.全局异常处理，及自定义返回统一json格式<br>
		7.jwt前后端分离的token认证<br>
		8.cors及跨域功能<br>
		9.shiro权限控制<br>
		10.activiti工作流<br>
		11.xss防火墙攻击<br>
		12.redis 双模式的存储<br>
#### 安装教程

1. 导入sql文件，即可部署项目

#### 使用说明

1. 使用mapper插件<br/>
	1). 一定要在实体对象中序列化，否则@GeneratedValue此注解不会生成序列或者uuid<br/>
	2). 实体类中不是数据库对应的字段，请加上@Transient注解，不然项目启动时会报错<br/>
2. 使用shiro注解的时候，不要在没有过滤url的controller类中使用
3. 使用mybatis-generator代码生成器，只需要在resources/mybatis-generator/mybatisGeneratorinit.properties文件中配置即可，然后在项目根目录运行mybatis-generator.cmd即可生成；注意，不要在IDE中运行，会找不到路径的
