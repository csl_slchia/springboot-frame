package com.frame.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
@ServletComponentScan
@MapperScan("com.frame.project.**.dao") //Mybatis DAO扫描
public class SpringbootFrameApplication extends SpringBootServletInitializer{
	
	/**
	 * 打包需继承SpringBootServletInitializer类，复写这个方法即可
	 */  
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringbootFrameApplication.class);
	}


	
	public static void main(String[] args) {
		SpringApplication.run(SpringbootFrameApplication.class, args);
	}
}
