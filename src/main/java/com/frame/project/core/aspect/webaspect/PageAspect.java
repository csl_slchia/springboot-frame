package com.frame.project.core.aspect.webaspect;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.util.Strings;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.github.pagehelper.PageHelper;

/**
 * 分页aop 切面注解
 * @author Administrator
 *
 */
@Order(1)
@Aspect
@Component
public class PageAspect {  
	
	private static final String pageNum = "pageNum"; //当前页数
	private static final String pageSize = "pageSize"; //每页显示多少条
	private static final String orderBy = "orderBy"; //排序字段
	
	/*
	 * 定义一个切入点
	 */
	@Pointcut("@annotation(com.frame.project.core.aspect.webaspect.PageAnnotation)")
	public void controllerAnnotation() {
	}


	// 用@Pointcut来注解一个切入方法，针对controller层有效
	@Pointcut("execution(public * com.frame.project..web..*.*(..))")
	public void excudeController() {
	}

	//@annotation 这个你应当知道指的是匹配注解
	//括号中的 annotation 并不是指所有自定标签，而是指在你的注释实现类中 *Aspect 中对应注解对象的别名，所以别被俩 annotation  所迷惑。
	@Around(value ="controllerAnnotation()&&excudeController()&&@annotation(annotation)")
	public Object twiceAsOld(ProceedingJoinPoint thisJoinPoint,
			PageAnnotation annotation) throws Throwable {
		Object result = null;
		System.out.println("分页切面："+annotation.value());  
		//获取request对象
		RequestAttributes ra = RequestContextHolder.getRequestAttributes();
		ServletRequestAttributes sra = (ServletRequestAttributes) ra;
		HttpServletRequest request = sra.getRequest();
		try {
			//从Header中取
			String pageNumHeader=  request.getHeader(pageNum);
			String pageSizeHeader =  request.getHeader(pageSize);
			String orderByHeader =  request.getHeader(orderBy);
			//从Parameter中取
			String pageNumParameter =  request.getParameter(pageNum);
			String pageSizeParameter =  request.getParameter(pageSize);
			String orderByParameter =  request.getParameter(orderBy);

			int pageNum = 1; //默认值
			int pageSize = 100; //默认值
			String orderBy = null;
			//第一种模式，pageNum和pageSize在Header中都存在，设置分页
			if(Strings.isNotEmpty(pageNumHeader)&&Strings.isNotEmpty(pageSizeHeader)) {
				 pageNum =  Integer.parseInt(pageNumHeader);
				 pageSize = Integer.parseInt(pageSizeHeader);
				 orderBy =  orderByHeader;
			}else if(Strings.isNotEmpty(pageNumParameter)&&Strings.isNotEmpty(pageSizeParameter)) 
			{//第二种模式，pageNum和pageSize在Parameter中存在，设置分页
				 pageNum =  Integer.parseInt(pageNumParameter);
				 pageSize = Integer.parseInt(pageSizeParameter);
				 orderBy =  orderByParameter;
			}
			
			//将参数传给这个方法就可以实现物理分页了，非常简单。
			PageHelper.startPage(pageNum, pageSize);
			//排序参数如果存在，就设置
			if(Strings.isNotEmpty(orderBy)) {
				PageHelper.orderBy(orderBy);
			}
			result = thisJoinPoint.proceed(); //环绕通知必须执行，否则不进入注解的方法
		} catch (Throwable e) {
			throw e;
		}
		return result;
	}
}
