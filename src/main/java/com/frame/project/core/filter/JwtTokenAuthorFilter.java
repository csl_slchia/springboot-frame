//package com.frame.project.core.filter;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import javax.security.auth.login.LoginException;
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.BeanFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.web.servlet.FilterRegistrationBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//import org.springframework.web.context.support.WebApplicationContextUtils;
//import com.frame.project.common.jwt.Audience;
//import com.frame.project.common.jwt.JwtHelper;
//import com.frame.project.core.exception.ErrorCode;
//import io.jsonwebtoken.Claims;
//
///**
// * token 校验
// * @author Administrator
// *
// */
//@Component
//public class JwtTokenAuthorFilter implements Filter {
//	private static Logger logger = LoggerFactory
//			.getLogger(JwtTokenAuthorFilter.class);
//
//	@Autowired
//	private Audience audience;
//
//
//	@Override
//	public void init(FilterConfig filterConfig) throws ServletException {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
//			throws IOException, ServletException {
//		HttpServletRequest request = (HttpServletRequest) req;
//		HttpServletResponse response = (HttpServletResponse) res;
//		//等到请求头信息authorization信息
//		final String authHeader = request.getHeader("token");
//		try {
//			if (authHeader == null || !authHeader.startsWith("bearer;")) {
//				throw new LoginException("token is not null");
//			}
//			final String token = authHeader.substring(7);
//			if(audience == null){
//				BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
//				audience = (Audience) factory.getBean("audience");
//			}
//			final Claims claims = JwtHelper.parseJWT(token,audience.getBase64Secret());
//			if(claims == null){
//				throw new LoginException("token不匹配");
//			}
//			chain.doFilter(req, res);
//			//request.setAttribute(Constants.CLAIMS, claims);
//		} catch (final Exception e) {
//			throw new ServletException(e.getMessage());
//		}
//	}
//
//
//	@Override
//	public void destroy() {
//		// TODO Auto-generated method stub
//
//	}
//
//	//注册filter
//	@Bean  
//	public FilterRegistrationBean<JwtTokenAuthorFilter>  filterRegistrationBean() {  
//		FilterRegistrationBean<JwtTokenAuthorFilter> registrationBean = new FilterRegistrationBean<>();  
//		JwtTokenAuthorFilter tokenAuthorFilter = new JwtTokenAuthorFilter();  
//		registrationBean.setFilter(tokenAuthorFilter);  
//		List<String> urlPatterns = new ArrayList<String>();  
//		urlPatterns.add("/api/*"); //只拦截带api名称的地址
//		registrationBean.setUrlPatterns(urlPatterns); 
//		registrationBean.setOrder(1);
//		return registrationBean;  
//	}  
//
//
//}
