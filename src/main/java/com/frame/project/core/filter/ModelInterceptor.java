/*package com.frame.project.core.filter;

import java.util.Enumeration;
import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Component;
*//**
 * 	使用介绍： Mybatis支持对Executor、StatementHandler、PameterHandler
	和ResultSetHandler接口进行拦截，也就是说会对这4种对象进行代理
	
 *  type：表示拦截的类，这里是Executor的实现类
 *  method：表示拦截的方法，这里是拦截Executor的query方法
 *  args：表示方法参数 List query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException;
 *  
 *  method参数：update，query，commit，rollback ....一些
 * @author Administrator
 *
 *//*
@Intercepts({
	@Signature(type=Executor.class,method="update",args={MappedStatement.class,Object.class}),
	@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class })})
@Component
public class ModelInterceptor implements Interceptor{
	
	*//**
	 * 处理代理类的执行
	 *//*
	@Override
	public Object intercept(Invocation arg0) throws Throwable {
		System.out.println(arg0);
		return arg0.proceed();
	}
	
	*//**
	 * 处理器(Handler)的构建过程
	 *//*
	@Override
	public Object plugin(Object arg0) {
		System.out.println(arg0);
		return Plugin.wrap(arg0, this);
	}
	
	*//**
	 * 拦截器属性的设置
	 *//*
	@Override
	public void setProperties(Properties arg0) {
		Enumeration<Object> keys = arg0.keys();
		while(keys.hasMoreElements()) {
			Object key = keys.nextElement();
			System.out.println(key+"-->"+arg0.get(key));
		}
	}

}
*/