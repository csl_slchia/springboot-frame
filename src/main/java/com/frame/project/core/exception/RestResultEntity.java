package com.frame.project.core.exception;

import lombok.Data;

@Data
public class RestResultEntity<T> {
	private boolean result; //返回状态false:异常，true正常
	private String message; //提示
	private String code;    //状态码
	private T data;			//对象数据



	public RestResultEntity() {

	}

	public RestResultEntity(CodeEnum errorCode) {
		this.code = errorCode.getCode();
		this.message = errorCode.getMessage();
		this.result = false;
		this.data = null;
	}


	public RestResultEntity(String code,String message) {
		this.code = code;
		this.message = message;
		this.result = false;
		this.data = null;
	}

	public static <T> RestResultEntity<T> newInstance() {
		return new RestResultEntity<>();
	}

}
