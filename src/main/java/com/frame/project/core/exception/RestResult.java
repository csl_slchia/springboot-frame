package com.frame.project.core.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RestResult {
	private static final  Logger LOGGER = LogManager.getLogger(RestResult.class);

	/**
	 * normal
	 * @param success
	 * @param data
	 * @param message
	 * @param <T>
	 * @return
	 */
	private static <T> RestResultEntity<T> genResult(boolean success, T data,Object obj,String message) {
		RestResultEntity<T> result = RestResultEntity.newInstance();
		result.setResult(success);
		result.setCode(obj instanceof CodeEnum ? ((CodeEnum)obj).getCode(): (String)obj);
		result.setData(data);
		result.setMessage(message);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("generate rest result:{}", result);
		}
		return result;
	}

	/**
	 * success
	 * @param data
	 * @param <T>
	 * @return
	 */
	public static <T> RestResultEntity<T> genResult(T data) {
		return genResult(true, data,CodeEnum.CODE_200, null);
	}
	
	
	/**
	 * success no message
	 * @return
	 */
	public static RestResultEntity<?> genResult() {
		return genResult(null);
	}
	

	/**
	 * error message
	 * @param message error message
	 * @param <T>
	 * @return
	 */
	public static <T> RestResultEntity<T> genErrorResult(String message) {
		return genResult(false, null, CodeEnum.CODE_500, message);
	}
	
	
	/**
	 * error message
	 * @param message error message
	 * @param <T>
	 * @return
	 */
	public static <T> RestResultEntity<T> genErrorResult(CodeEnum codeEnum) {
		return genResult(false, null, codeEnum, codeEnum.getMessage());
	}
	

	/**
	 * error
	 * @param error error enum,message
	 * @param <T>
	 * @return
	 */
	public static <T> RestResultEntity<T> genErrorResult(CodeEnum error,String message) {
		return genResult(false, null,error, message);
	}
	
	/**
	 * error
	 * @param error error code,message
	 * @param <T>
	 * @return
	 */
	public static <T> RestResultEntity<T> genErrorResult(String code,String message) {
		return genResult(false, null,code, message);
	}

	

}
