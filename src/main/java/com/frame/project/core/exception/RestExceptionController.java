package com.frame.project.core.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 全局异常  json显示
 * @author Administrator
 *
 */
@RestController
public class RestExceptionController  {

	private  static Logger logger = LogManager.getLogger(RestExceptionController.class);

	
	@RequestMapping(path = RestExceptionHandler.errorUrl)
    public  RestResultEntity<?> errorBody(HttpServletResponse res,HttpServletRequest req) {
		res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		RestResultEntity<?> restResult = (RestResultEntity<?>)req.getAttribute(RestExceptionHandler.attributeName);
        return restResult;
    }
	
	
	@RequestMapping(path = "/unauth")
    public  RestResultEntity<?> unauth(HttpServletResponse res,HttpServletRequest req) {
		//res.setStatus(HttpServletResponse.SC_OK);
        return RestResult.genErrorResult(CodeEnum.CODE_401);
    }
	
	

}
