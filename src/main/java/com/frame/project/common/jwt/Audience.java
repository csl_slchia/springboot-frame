package com.frame.project.common.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * 配置信息的实体类，以便获取jwt的配置
 * @author Administrator
 *
 */
@Data
@ConfigurationProperties(prefix = "audience")
@Component
public class Audience {
	private String clientId;
	private String base64Secret;
	private String name;
	private int expiresSecond;
	private String header;
}
