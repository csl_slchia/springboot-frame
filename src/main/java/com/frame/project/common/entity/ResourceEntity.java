package com.frame.project.common.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 公共bean
 * @author Administrator
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(generator="UUID")
	private String id; //主键id
	
}
