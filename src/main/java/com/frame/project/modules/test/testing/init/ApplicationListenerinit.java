package com.frame.project.modules.test.testing.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.frame.project.modules.test.testing.service.TestService;

/**
 * 交给Spring管理，如果不是自动扫描加载bean的方式，则在xml里配一个即可
 * @author Administrator
 *
 */
@Component
public class ApplicationListenerinit implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	TestService testService;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		/**
		 * 在web 项目中（spring mvc），系统会存在两个容器，
		 * 一个是root application context ,
		 * 另一个就是我们自己的 projectName-servlet context（作为root application context的子容器）。
		 * 这种情况下，就会造成onApplicationEvent方法被执行两次。
		 * 为了避免上面提到的问题，我们可以只在root application context初始化完成后调用逻辑代码，
		 * 其他的容器的初始化完成，则不做任何处理,写成event.getApplicationContext().getParent() == null即可
		 */
		if (event.getApplicationContext().getParent() == null) {
			System.out.println("ApplicationListener监听初始化，分页size为："+testService.findList().getPageSize());
		}

	}


}
