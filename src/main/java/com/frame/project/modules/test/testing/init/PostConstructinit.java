package com.frame.project.modules.test.testing.init;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.project.modules.test.testing.service.TestService;

/**
 * 第二种初始化： spring初始化 bean完成后，会调用这个初始化接口，下面演示初始化
 * @author Administrator
 *
 */
@Component
public class PostConstructinit {
	
	@Autowired
	TestService testService;
	
	@PostConstruct
	public void init() {
		System.out.println("@PostConstruct注解初始化，分页size为："+testService.findList().getPageSize());
	}
}
