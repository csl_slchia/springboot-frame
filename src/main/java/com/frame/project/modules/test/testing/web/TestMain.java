package com.frame.project.modules.test.testing.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.dom4j.Attribute;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ImageUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;

public class TestMain {

	
	/**
	 * dom4j  修改xml中的属性
	 * @throws DocumentException 
	 * @throws IOException 
	 */
	@Test
	public  void updateXML() throws DocumentException, IOException {
		String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><actrolestore>"
				+ "<actroles id=\"111\"><lcdyKey>purchase</lcdyKey><nodeId>financeUserList</nodeId><nodeUses>test1</nodeUses><title>采购管理</title></actroles>"
				+"<actroles id=\"111\"><lcdyKey>purchase</lcdyKey><nodeId>financeUserList</nodeId><nodeUses>test2</nodeUses><title>采购管理</title></actroles>"
				+ "</actrolestore>"; 
		org.dom4j.Document document = DocumentHelper.parseText(xmlStr);
		Element root = document.getRootElement();
		            
		Iterator<?> ruleNodes = root.elementIterator("actroles");
		while (ruleNodes.hasNext()) {
		        Element ruleElement = (Element) ruleNodes.next();
		        // 修改isSelf的属性值
		       Attribute isSelfAttr = ruleElement.attribute("id");
		       isSelfAttr.setValue("222");                
		}
		// 格式化输出格式
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setEncoding("utf-8");
		StringWriter writer = new StringWriter();
		// 格式化输出流
		XMLWriter xmlWriter = new XMLWriter(writer, format);
		//将document写入到输出流
		xmlWriter.write(document);
		xmlWriter.close();
		System.out.println(writer.toString());
	}
	
	
	/**
	 * javax 解析xml
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	@Test
	public  void javaxXML() throws ParserConfigurationException, SAXException, IOException {
		String xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" + 
				"\r\n" + 
				"<actrolestore> \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>jszc</nodeId>  \r\n" + 
				"    <nodeUses>test1</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>xmfjl</nodeId>  \r\n" + 
				"    <nodeUses>test2</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>xmjl</nodeId>  \r\n" + 
				"    <nodeUses>test3</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>xnygsba</nodeId>  \r\n" + 
				"    <nodeUses>test4</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>zbbm</nodeId>  \r\n" + 
				"    <nodeUses>test5</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>jhfzb</nodeId>  \r\n" + 
				"    <nodeUses>test6</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>fgfzjl</nodeId>  \r\n" + 
				"    <nodeUses>test7</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>zjl</nodeId>  \r\n" + 
				"    <nodeUses>test8</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles>  \r\n" + 
				"  <actroles id=\"111\"> \r\n" + 
				"    <lcdyKey>cglx</lcdyKey>  \r\n" + 
				"    <nodeId>gxfgszjl</nodeId>  \r\n" + 
				"    <nodeUses>test9</nodeUses>  \r\n" + 
				"    <title>采购立项</title> \r\n" + 
				"  </actroles> \r\n" + 
				"</actrolestore>\r\n" + 
				""; 
		xmlStr = xmlStr.replaceAll("\r | \n", "");
		StringReader sr = new StringReader(xmlStr); 
		InputSource is = new InputSource(sr); 
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
		DocumentBuilder builder=factory.newDocumentBuilder(); 
		Document document = builder.parse(is); 
		NodeList actList = document.getElementsByTagName("actroles");
		
		//遍历actRoles
		for(int i=0;i<actList.getLength();i++){
			//获取第i个actRole结点
			org.w3c.dom.Node node = actList.item(i);
			//获取第i个actRole的所有属性
			NamedNodeMap namedNodeMap = node.getAttributes();
			//获取已知名为id的属性值
			String id = namedNodeMap.getNamedItem("id").getTextContent();//System.out.println(id);
			//获取actRole结点的子节点,包含了Test类型的换行
			NodeList cList = node.getChildNodes();//System.out.println(cList.getLength());9

			//将一个actRole里面的属性加入数组
			ArrayList<String> contents = new ArrayList<>();
			for(int j=1;j<cList.getLength();j+=2){
				org.w3c.dom.Node cNode = cList.item(j);
				String content = cNode.getFirstChild().getTextContent();
				contents.add(content);
			}
			System.out.println(
					contents.get(0)+","+
					contents.get(1)+","+
					contents.get(2)+","+
					contents.get(3)+","
				
			);
			
			
			
		}	
	}


	@Test
	public  void bascUtil() {
		String content = UUID.randomUUID()+"test中文";
		// 随机生成密钥 
		byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
		byte[] key1 = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

		// 构建
		AES aes = SecureUtil.aes(key);
		AES aes1 = SecureUtil.aes(key1);

		// 加密
		byte[] encrypt = aes.encrypt(content);
		byte[] encrypt1 = aes1.encrypt(content);
		// 解密
		//aes.decrypt(encrypt);

		// 加密为16进制表示
		//String encryptHex = aes.encryptHex(content);

		// 解密为字符串
		String decryptStr = aes.decryptStr(encrypt, CharsetUtil.CHARSET_UTF_8);
		System.out.println("元字符串："+content+"\n"+"key："+key+"\n"+"加密："+encrypt+"\n"+"解密："+decryptStr);

	}




	@Test
	public  void imageUtil() {
		//图片转换黑白
		ImageUtil.gray(new File("F:\\项目设计\\思恒达公司\\eqs\\001界面\\校\\登录_看图王.jpg"),
				new File("F:\\项目设计\\思恒达公司\\eqs\\001界面\\校\\登录_看图王copy.jpg"));
	}




	@Test
	public void FileTypeUtil() {
		File[] ls = FileUtil.ls("C:\\Users\\Administrator\\Desktop\\zhfd");
		for (File file : ls) {
			String type = FileTypeUtil.getType(file);
			System.out.println(type);
		}

	}



	@Test
	public void FileUtil() {
		File[] ls = FileUtil.ls("C:\\Users\\Administrator\\Desktop\\zhfd");
		for (File file : ls) {
			System.out.println(file);
		}

	}

	@Test
	public void IoUtil() {
		BufferedInputStream in = FileUtil.getInputStream("C:\\Users\\Administrator\\Desktop\\zhfd\\新建文本文档.txt");
		BufferedOutputStream out = FileUtil.getOutputStream("C:\\Users\\Administrator\\Desktop\\zhfd\\新建文本文档copy.txt");
		long copySize = IoUtil.copy(in, out, IoUtil.DEFAULT_BUFFER_SIZE);
		IoUtil.close(in);
		IoUtil.close(out);
		System.out.println(copySize);
	}



	@Test
	public void DateUtil() {
		//当前时间
		DateTime date = DateUtil.date();
		System.out.println("date："+date);
		//当前时间
		Date date2 = DateUtil.date(Calendar.getInstance());
		System.out.println("date2："+date2);
		//当前时间
		Date date3 = DateUtil.date(System.currentTimeMillis());
		System.out.println("date3："+date3);
		//当前时间字符串，格式：yyyy-MM-dd HH:mm:ss
		String now = DateUtil.now();
		System.out.println("now："+now);

		//当前日期字符串，格式：yyyy-MM-dd
		String today= DateUtil.today();
		System.out.println("today："+today);

		String format = DateUtil.format(date, "yyyy-MM-dd");
		System.out.println("format："+format);
	}
}
