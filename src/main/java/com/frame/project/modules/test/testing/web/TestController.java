package com.frame.project.modules.test.testing.web;


import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.frame.project.common.RedisUtil;
import com.frame.project.common.StringRedisUtil;
import com.frame.project.core.aspect.webaspect.PageAnnotation;
import com.frame.project.core.exception.RestResultEntity;
import com.frame.project.core.exception.RestResult;
import com.frame.project.modules.test.sys.dao.SysUserDao;
import com.frame.project.modules.test.sys.entity.SysUser;
import com.frame.project.modules.test.testing.service.TestService;
import com.github.pagehelper.PageInfo;


@RequestMapping("api/test")
@RestController 
public class TestController {

	@Value("${server.port}")
	String port;

	@Autowired
	TestService testService;
	
	@Autowired
	private SysUserDao sysUserDao;

	
	@RequestMapping({"list"})
	public RestResultEntity<?> one() {
		return RestResult.genResult(sysUserDao.selectAll());
	}

	@RequestMapping("/queryXML")
	public Object  queryXML() throws IOException, DocumentException{
		String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><actrolestore>"
				+ "   <actroles id=\"111\"><lcdyKey>purchase</lcdyKey><nodeId>financeUserList</nodeId><nodeUses>test1</nodeUses><title>采购管理</title></actroles>  "
				+"  <actroles id=\"111\"><lcdyKey>purchase</lcdyKey><nodeId>financeUserList</nodeId><nodeUses>test2</nodeUses><title>采购管理</title></actroles>  "
				+ "  </actrolestore>"; 
		Document document = null;
		document = DocumentHelper.parseText(str);
		// 格式化输出格式
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setEncoding("utf-8");
		StringWriter writer = new StringWriter();
		// 格式化输出流
		XMLWriter xmlWriter = new XMLWriter(writer, format);
		//将document写入到输出流
		xmlWriter.write(document);
		xmlWriter.close();
		return RestResult.genResult(writer.toString());
	}


	@PageAnnotation("controller")
	@RequestMapping("/findList")
	public RestResultEntity<?>  findList(){
		return RestResult.genResult(testService.findList());
	}


	@RequestMapping("/article")
	public RestResultEntity<?> article() {
		RedisUtil.set("name", testService.findList(),60); //60秒后过期
		PageInfo<SysUser> list = RedisUtil.get("name",PageInfo.class);
		return RestResult.genResult(list);
	}

	@RequestMapping("/require_auth")
	@RequiresAuthentication
	public RestResultEntity<?> requireAuth() {
		return  RestResult.genResult("登入的用户才可以进行访问,端口号:"+port);
	}

	@RequestMapping("/require_role")
	@RequiresRoles("admin")
	public RestResultEntity<?> requireRole() {
		return RestResult.genResult("admin的角色用户才可以登入");
	}

	@RequestMapping("/require_permissionAll")
	@RequiresPermissions(logical = Logical.AND, value = {"add","view","delete","edit"})
	public RestResultEntity<?> requirePermissionAll() {
		return RestResult.genResult("拥有add和view、delete、edit权限的用户才可以访问");
	}


	@RequestMapping("/require_permission")
	@RequiresPermissions(logical = Logical.AND, value = {"add","view"})
	public RestResultEntity<?> requirePermission() {
		return RestResult.genResult("拥有add和view权限的用户才可以访问");
	}



}
