package com.frame.project.modules.test.sys.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.frame.project.common.jwt.Audience;
import com.frame.project.common.jwt.JwtHelper;
import com.frame.project.core.exception.CodeEnum;
import com.frame.project.core.exception.RestResult;
import com.frame.project.modules.test.sys.entity.SysUser;
import com.frame.project.modules.test.sys.service.SysUserService;


/**
 * 用户登录
 * @author Administrator
 *
 */
@RestController 
public class LoginController {

	private static final  Logger logger = LogManager.getLogger(LoginController.class);

	@Autowired
	private SysUserService sysUserService;
	

	@Autowired
	private Audience audience;
	

	@RequestMapping({"login"})
	public Object login(SysUser user,HttpServletRequest request) throws Exception {
		user = sysUserService.getUser(user);
		if (user == null) {
			return RestResult.genErrorResult(CodeEnum.CODE_401,"用户或密码错误"); 
		}
		//生成token
		String jwtToken = JwtHelper.createJWT(user.getName(),
				user.getId(),
				user.getRoleNames(),
				audience.getClientId(),
				audience.getName(),
				audience.getExpiresSecond(),
				audience.getBase64Secret());

		String result_str = "bearer;" + jwtToken;
		return RestResult.genResult(result_str);
	}
	
}
