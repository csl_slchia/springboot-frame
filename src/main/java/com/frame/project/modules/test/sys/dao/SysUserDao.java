package com.frame.project.modules.test.sys.dao;

import java.util.List;
import com.frame.project.common.entity.ResourceMapper;
import com.frame.project.modules.test.sys.entity.SysUser;
/**
 * 用户dao
 * @author Administrator
 *
 */

public interface SysUserDao  extends ResourceMapper<SysUser>{


    //查询所有
    List<SysUser> findUserList();
	
	SysUser getUser(SysUser sysUser);
    
    Integer count();
	
}
