package com.frame.project.modules.test.testing.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.frame.project.modules.test.sys.dao.SysUserDao;
import com.frame.project.modules.test.sys.entity.SysUser;
import com.github.pagehelper.PageInfo;

@Service
@CacheConfig(cacheNames="sysuser")
public class TestService {
	
	@Autowired
	private SysUserDao sysUserDao;
	
	
	/**
	 * redis缓存
	 * @return
	 */
	//@Cacheable
	public PageInfo<SysUser>  findList(){
		List<SysUser> userDomains = sysUserDao.findUserList();
		PageInfo<SysUser> result = new PageInfo<SysUser>(userDomains); 
		return result;
	}
}
