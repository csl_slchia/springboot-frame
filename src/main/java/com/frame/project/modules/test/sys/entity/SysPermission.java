package com.frame.project.modules.test.sys.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.frame.project.common.entity.ResourceEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 权限表
 * @author Administrator
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sys_permission")
public class SysPermission extends ResourceEntity{
	
	private static final long serialVersionUID = 1L;

	@Column
	private String name; //权限名称

}
