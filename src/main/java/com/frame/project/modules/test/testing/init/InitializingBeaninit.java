package com.frame.project.modules.test.testing.init;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.project.modules.test.testing.service.TestService;

/**
 * 第一种初始化：spring初始化 bean完成后，会调用这个初始化接口，下面演示初始化
 * @author Administrator
 *
 */
@Component
public class InitializingBeaninit implements InitializingBean {
	@Autowired
	TestService testService;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("InitializingBean接口初始化，分页size为："+testService.findList().getPageSize());
		
	}
	
	
	



}
