package com.frame.project.modules.test.sys.dao;

import java.util.List;

import com.frame.project.common.entity.ResourceMapper;
import com.frame.project.modules.test.sys.entity.SysPermission;
import com.frame.project.modules.test.sys.entity.SysRole;

/**
 * 权限dao
 * @author Administrator
 *
 */

public interface SysPermissionDao extends ResourceMapper<SysPermission>{

    List<SysPermission> findRolePermissionList(SysRole sysRole);
	
}
