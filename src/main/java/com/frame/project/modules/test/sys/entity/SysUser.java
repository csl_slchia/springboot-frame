package com.frame.project.modules.test.sys.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.frame.project.common.entity.ResourceEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Administrator
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sys_user")
public class SysUser extends ResourceEntity{

	private static final long serialVersionUID = 1L;

    @NotNull(message="name不能为空") //空字符串可以，不能为null
	@Column
	private String name; //用户名
	
    @NotEmpty(message = "密码不能为空")
  	@Length(min = 6, max = 8, message = "密码长度为6-8位。")
  	@Pattern(regexp = "[a-zA-Z]*", message = "密码不合法")
	@Column
	private String pwd; //密码
	
	@Column
	private String email; //邮箱
	
	@Column
	private String phone; //手机号
	
	@Transient
	private String roleNames; //角色名称，临时值
	
	@Transient
	private List<String> roles; 
	
	@Transient
	private List<SysRole> sysRole;
	
	@Transient
	private Set<String> sysPermission; //权限
	
}
