package com.frame.project.modules.test.testing.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.frame.project.modules.test.testing.entity.SysUser;

import de.odysseus.el.ExpressionFactoryImpl;
import de.odysseus.el.util.SimpleContext;

@RestController
@RequestMapping("/activitiSequenceFlow")
public class ActivitiSequenceFlowController {

	@Autowired
	ProcessEngine processEngine; //核心API,其他的类都是由他而来

	@Autowired
	ObjectMapper objectMapper; //jackson提供的,对象转换成为一个json字符串


	/**
	 * 部署流程
	 */
	@RequestMapping("/deployment")
	public void deployment(){
		Deployment deploy = processEngine.getRepositoryService() //与流程定义和部署相关的service
				.createDeployment() //创建一个部署对象
				.name("sequenceFlow连线") //设置部署名称
				.addClasspathResource("processes/sequenceFlow.bpmn")//从classpath的资源中加载,一次只能加载一个文件
				.addClasspathResource("processes/sequenceFlow.png")
				.deploy(); //完成部署
		System.out.println(deploy.getId());    
		System.out.println(deploy.getName()); //部署的名称
	}


	/**
	 * 启动流程实例
	 */
	@RequestMapping("/start")
	public void start(){
		ProcessInstance processInstance = processEngine.getRuntimeService() //与正在执行的流程实例和执行对象相关的service
				.startProcessInstanceByKey("sequenceFlow"); //使用流程定义的key启动流程实例，key对应的是流程图中的id属性,默认按照最新流程版本启动。
		System.out.println("流程实例id:"+processInstance.getId());//流程实例id
		System.out.println("流程定义id"+processInstance.getProcessDefinitionId()); //流程定义id
	}
	
	
	/**
	 * 完成我的任务
	 */
	@RequestMapping("/completeMyPersonaiTask/{id}/{massage}")
	public void completeMyPersonaiTask(@PathVariable("id")String taskId,@PathVariable("massage")String massage){
		Map<String, Object> map = new HashMap<>();
		map.put("massage", massage);
		processEngine.getTaskService()
		.complete(taskId,map);
		System.out.println("完成任务,任务id:"+taskId);
	}

	
	

	/**
	 * 查询当前人的任务
	 */
	@RequestMapping("/findMyPersonaiTask/{taskAssignee}")
	public void findMyPersonaiTask(@PathVariable("taskAssignee")String taskAssignee){
		List<Task> list = processEngine.getTaskService() //与正在执行的任务相关的service
				.createTaskQuery()  //创建任务查询对象
				.taskAssignee(taskAssignee) //指定办理人
				.list();
		for (Task task : list) {
			System.out.println("任务id:"+task.getId());
			System.out.println("任务名称:"+task.getName());
			System.out.println("任务创建时间:"+task.getCreateTime());
			System.out.println("任务的办理人:"+task.getAssignee());
			System.out.println("流程实例id:"+task.getProcessInstanceId());
			System.out.println("执行对象id:"+task.getExecutionId());
			System.out.println("流程定义id:"+task.getProcessDefinitionId());
		}
	}





}
