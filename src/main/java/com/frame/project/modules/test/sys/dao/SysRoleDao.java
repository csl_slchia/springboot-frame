package com.frame.project.modules.test.sys.dao;

import java.util.List;

import com.frame.project.common.entity.ResourceMapper;
import com.frame.project.modules.test.sys.entity.SysRole;
import com.frame.project.modules.test.sys.entity.SysUser;

/**
 * 角色 dao
 * @author Administrator
 *
 */

public interface SysRoleDao extends ResourceMapper<SysRole>{

    List<SysRole> findRoleList();
    
    List<SysRole> findUserRoleList(SysUser sysUser);
	
}
