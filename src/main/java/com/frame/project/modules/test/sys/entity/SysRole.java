package com.frame.project.modules.test.sys.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.frame.project.common.entity.ResourceEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 角色表
 * @author Administrator
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sys_role")
public class SysRole extends ResourceEntity{
	
	private static final long serialVersionUID = 1L;

	@Column
	private String name; //角色名称
	
	@Transient
	private List<SysPermission> sysPermission; //权限
}
