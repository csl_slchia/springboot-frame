prompt PL/SQL Developer import file
prompt Created on 2018年7月24日 by Administrator
set feedback off
set define off
prompt Creating SYS_PERMISSION...
create table SYS_PERMISSION
(
  id   VARCHAR2(36) not null,
  name VARCHAR2(50)
)
;
comment on column SYS_PERMISSION.id
  is '主键id';
comment on column SYS_PERMISSION.name
  is '权限名称';
alter table SYS_PERMISSION
  add constraint PK_PERMISSION primary key (ID);

prompt Creating SYS_PERMISSION_ROLE...
create table SYS_PERMISSION_ROLE
(
  id                VARCHAR2(36) not null,
  sys_role_id       VARCHAR2(36),
  sys_permission_id VARCHAR2(36)
)
;
comment on column SYS_PERMISSION_ROLE.id
  is '主键id';
comment on column SYS_PERMISSION_ROLE.sys_role_id
  is '角色id';
comment on column SYS_PERMISSION_ROLE.sys_permission_id
  is '权限id';
alter table SYS_PERMISSION_ROLE
  add constraint PK_PP primary key (ID);

prompt Creating SYS_RESOURCES...
create table SYS_RESOURCES
(
  id          VARCHAR2(36) not null,
  pid         VARCHAR2(36),
  name        VARCHAR2(50),
  url         VARCHAR2(100),
  descritpion VARCHAR2(50)
)
;
comment on column SYS_RESOURCES.id
  is '主键id';
comment on column SYS_RESOURCES.pid
  is '父级id';
comment on column SYS_RESOURCES.name
  is '权限名称';
comment on column SYS_RESOURCES.url
  is '权限url';
comment on column SYS_RESOURCES.descritpion
  is '权限描述';
alter table SYS_RESOURCES
  add constraint PK_RESOURCES primary key (ID);

prompt Creating SYS_RESOURCES_ROLE...
create table SYS_RESOURCES_ROLE
(
  id             VARCHAR2(36) not null,
  sys_role_id    VARCHAR2(36),
  sys_permission VARCHAR2(36)
)
;
comment on column SYS_RESOURCES_ROLE.id
  is '主键id';
comment on column SYS_RESOURCES_ROLE.sys_role_id
  is '角色id';
comment on column SYS_RESOURCES_ROLE.sys_permission
  is '权限id';
alter table SYS_RESOURCES_ROLE
  add constraint PK_PR_ID primary key (ID);

prompt Creating SYS_ROLE...
create table SYS_ROLE
(
  id   VARCHAR2(36) not null,
  name VARCHAR2(50)
)
;
comment on column SYS_ROLE.id
  is '主键id';
comment on column SYS_ROLE.name
  is '角色名称';
alter table SYS_ROLE
  add constraint PK_ROLE_ID primary key (ID);

prompt Creating SYS_ROLE_USER...
create table SYS_ROLE_USER
(
  id          VARCHAR2(36) not null,
  sys_user_id VARCHAR2(36),
  sys_role_id VARCHAR2(36)
)
;
comment on column SYS_ROLE_USER.id
  is '主键id';
comment on column SYS_ROLE_USER.sys_user_id
  is '用户id';
comment on column SYS_ROLE_USER.sys_role_id
  is '角色id';
alter table SYS_ROLE_USER
  add constraint PK_UR_ID primary key (ID);

prompt Creating SYS_USER...
create table SYS_USER
(
  id    VARCHAR2(36) not null,
  name  VARCHAR2(50),
  pwd   VARCHAR2(20),
  email VARCHAR2(20),
  phone VARCHAR2(20)
)
;
comment on column SYS_USER.id
  is '主键id';
comment on column SYS_USER.name
  is '用户名';
comment on column SYS_USER.pwd
  is '密码';
comment on column SYS_USER.email
  is '邮箱';
comment on column SYS_USER.phone
  is '手机号';
alter table SYS_USER
  add constraint PK_USER_ID primary key (ID);

prompt Loading SYS_PERMISSION...
insert into SYS_PERMISSION (id, name)
values ('1', 'add');
insert into SYS_PERMISSION (id, name)
values ('2', 'view');
insert into SYS_PERMISSION (id, name)
values ('3', 'delete');
insert into SYS_PERMISSION (id, name)
values ('4', 'edit');
commit;
prompt 4 records loaded
prompt Loading SYS_PERMISSION_ROLE...
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('1', '1', '1');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('2', '1', '2');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('3', '1', '3');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('4', '1', '4');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('5', '8', '1');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('6', '8', '2');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('7', '2', '1');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('8', '2', '2');
insert into SYS_PERMISSION_ROLE (id, sys_role_id, sys_permission_id)
values ('9', '2', '4');
commit;
prompt 9 records loaded
prompt Loading SYS_RESOURCES...
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('100', '0', '菜单大全', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('10001', '1001', '菜单01-二级菜单01', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('10002', '1001', '菜单01-二级菜单02', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('10003', '1001', '菜单01-二级菜单03', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('1001', '100', '一级菜单01', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('1002', '100', '一级菜单02', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('1003', '100', '一级菜单03', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('20001', '1002', '菜单02-二级菜单01', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('20002', '1002', '菜单02-二级菜单02', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('20003', '1002', '菜单02-二级菜单03', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('30001', '1003', '菜单03-二级菜单01', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('30002', '1003', '菜单03-二级菜单02', null, null);
insert into SYS_RESOURCES (id, pid, name, url, descritpion)
values ('30003', '1003', '菜单03-二级菜单03', null, null);
commit;
prompt 13 records loaded
prompt Loading SYS_RESOURCES_ROLE...
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('01', '1', '1001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('02', '1', '10001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('03', '1', '10002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('04', '1', '10003');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('05', '1', '1002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('06', '1', '20001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('07', '1', '20002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('08', '1', '20003');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('09', '1', '1003');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('10', '1', '30001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('11', '1', '30002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('12', '1', '30003');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('13', '8', '1001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('14', '8', '10001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('15', '8', '10002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('16', '8', '10003');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('17', '8', '1002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('18', '8', '20001');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('19', '8', '20002');
insert into SYS_RESOURCES_ROLE (id, sys_role_id, sys_permission)
values ('20', '8', '20003');
commit;
prompt 20 records loaded
prompt Loading SYS_ROLE...
insert into SYS_ROLE (id, name)
values ('1', 'admin');
insert into SYS_ROLE (id, name)
values ('2', '管理员');
insert into SYS_ROLE (id, name)
values ('3', '钻石用户');
insert into SYS_ROLE (id, name)
values ('4', '白金用户');
insert into SYS_ROLE (id, name)
values ('5', '黄金用户');
insert into SYS_ROLE (id, name)
values ('6', '白银用户');
insert into SYS_ROLE (id, name)
values ('7', '青铜用户');
insert into SYS_ROLE (id, name)
values ('8', '普通用户');
commit;
prompt 8 records loaded
prompt Loading SYS_ROLE_USER...
insert into SYS_ROLE_USER (id, sys_user_id, sys_role_id)
values ('1', '11111', '8');
insert into SYS_ROLE_USER (id, sys_user_id, sys_role_id)
values ('2', '2222', '3');
insert into SYS_ROLE_USER (id, sys_user_id, sys_role_id)
values ('3', '333', '3');
insert into SYS_ROLE_USER (id, sys_user_id, sys_role_id)
values ('4', '4444', '4');
insert into SYS_ROLE_USER (id, sys_user_id, sys_role_id)
values ('5', '9999', '1');
commit;
prompt 5 records loaded
prompt Loading SYS_USER...
insert into SYS_USER (id, name, pwd, email, phone)
values ('11111', '张三', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('2222', '李四', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('333', '王五', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('4444', '赵柳', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('5555', '小小', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('66666', '钱琪', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('77777', '小宋', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('88888', '小徐', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('9999', 'admin', '123', '18412212@qq.com', '13244567866');
insert into SYS_USER (id, name, pwd, email, phone)
values ('10001', 'oracle用户', '123', '18412212@qq.com', '13244567866');
commit;
prompt 10 records loaded
set feedback on
set define on
prompt Done.
