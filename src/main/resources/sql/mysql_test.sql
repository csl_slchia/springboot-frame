/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.5.60 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`test` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `test`;

/*Table structure for table `sys_permission` */

DROP TABLE IF EXISTS `sys_permission`;

CREATE TABLE `sys_permission` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `name` varchar(36) DEFAULT NULL COMMENT '权限名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_permission` */

insert  into `sys_permission`(`id`,`name`) values ('1','add'),('2','view'),('3','delete'),('4','edit');

/*Table structure for table `sys_permission_role` */

DROP TABLE IF EXISTS `sys_permission_role`;

CREATE TABLE `sys_permission_role` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `sys_role_id` varchar(36) DEFAULT NULL COMMENT '角色id',
  `sys_permission_id` varchar(36) DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_permission_role` */

insert  into `sys_permission_role`(`id`,`sys_role_id`,`sys_permission_id`) values ('1','1','1'),('2','1','2'),('3','1','3'),('4','1','4'),('5','8','1'),('6','8','2'),('7','2','1'),('8','2','2'),('9','2','4');

/*Table structure for table `sys_resources` */

DROP TABLE IF EXISTS `sys_resources`;

CREATE TABLE `sys_resources` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `pid` varchar(36) DEFAULT NULL COMMENT '父级id',
  `name` varchar(100) DEFAULT NULL COMMENT '权限名称',
  `descritpion` varchar(100) DEFAULT NULL COMMENT '权限描述',
  `url` varchar(100) DEFAULT NULL COMMENT '权限url',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_resources` */

insert  into `sys_resources`(`id`,`pid`,`name`,`descritpion`,`url`) values ('100','0','菜单大全',NULL,NULL),('10001','1001','菜单01-二级菜单01',NULL,NULL),('10002','1001','菜单01-二级菜单02',NULL,NULL),('10003','1001','菜单01-二级菜单03',NULL,NULL),('1001','100','一级菜单01',NULL,NULL),('1002','100','一级菜单02',NULL,NULL),('1003','100','一级菜单03',NULL,NULL),('20001','1002','菜单02-二级菜单01',NULL,NULL),('20002','1002','菜单02-二级菜单02',NULL,NULL),('20003','1002','菜单02-二级菜单03',NULL,NULL),('30001','1003','菜单03-二级菜单01',NULL,NULL),('30002','1003','菜单03-二级菜单02',NULL,NULL),('30003','1003','菜单03-二级菜单03',NULL,NULL);

/*Table structure for table `sys_resources_role` */

DROP TABLE IF EXISTS `sys_resources_role`;

CREATE TABLE `sys_resources_role` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `sys_role_id` varchar(36) DEFAULT NULL COMMENT '角色id',
  `sys_permission_id` varchar(36) DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_resources_role` */

insert  into `sys_resources_role`(`id`,`sys_role_id`,`sys_permission_id`) values ('01','1','1001'),('02','1','10001'),('03','1','10002'),('04','1','10003'),('05','1','1002'),('06','1','20001'),('07','1','20002'),('08','1','20003'),('09','1','1003'),('10','1','30001'),('11','1','30002'),('12','1','30003'),('13','8','1001'),('14','8','10001'),('15','8','10002'),('16','8','10003'),('17','8','1002'),('18','8','20001'),('19','8','20002'),('20','8','20003');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`) values ('1','admin'),('2','管理员'),('3','钻石用户'),('4','白金用户'),('5','黄金用户'),('6','白银用户'),('7','青铜用户'),('8','普通用户');

/*Table structure for table `sys_role_user` */

DROP TABLE IF EXISTS `sys_role_user`;

CREATE TABLE `sys_role_user` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `sys_user_id` varchar(36) DEFAULT NULL COMMENT '用户id',
  `sys_role_id` varchar(36) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_user` */

insert  into `sys_role_user`(`id`,`sys_user_id`,`sys_role_id`) values ('1','11111','8'),('3','2222','3'),('4','333','3'),('5','4444','4'),('6','9999','1');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` varchar(36) NOT NULL COMMENT '主键id',
  `name` varchar(100) DEFAULT NULL COMMENT '用户名',
  `pwd` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(100) DEFAULT NULL COMMENT '手机号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`name`,`pwd`,`email`,`phone`) values ('100001','mysql用户','123','18412212@qq.com','13244567866'),('11111','张三','123','18412212@qq.com','13244567866'),('2222','李四','123','18412212@qq.com','13244567866'),('333','王五','123','18412212@qq.com','13244567866'),('4444','赵柳','123','18412212@qq.com','13244567866'),('5555','小小','123','18412212@qq.com','13244567866'),('66666','钱琪','123','18412212@qq.com','13244567866'),('77777','小宋','123','18412212@qq.com','13244567866'),('88888','小徐','123','18412212@qq.com','13244567866'),('9999','admin','123','18412212@qq.com','13244567866');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
